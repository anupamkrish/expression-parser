'''
This program implements a recursive descent parser for the CFG below:

Syntax Rule  Lookahead Set          Strings generated
------------------------------------------------------------
1 <exp> → <term>{+<term> | -<term>}
2 <term> → <factor>{*<factor> | /<factor>}
3 <factor> → (<exp>) | pi | <func>(<exp>) | <atomic> # Commented out: -<factor>
4 <func> → sin | cos | tan | sqrt
0 <statement> → table | show <exp>{,<exp>} | <id> = <exp>

<func> → sin | cos | tan | sqrt
'''
import math
import operator

class ParseError(Exception): pass



#==============================================================
# FRONT END PARSER
#==============================================================

i = 0 # keeps track of what character we are currently reading.
err = None
func_map = {
    "sin": math.sin,
    "cos": math.cos,
    "tan": math.tan,
    "sqrt": math.sqrt,
    "fact": math.factorial,
    "log": math.log,
    "+": operator.add,
    "-": operator.sub,
    "*": operator.mul,
    "/": operator.truediv,
    "//": operator.floordiv,
    "**": operator.pow,
    "%": operator.mod

}
assignment = {}

#---------------------------------------
# Parse Statement   <statement> → table | show <exp>{,<exp>} | <id> = <exp>
#---------------------------------------
def statement():
    global i, err
    # Parse 'table' 
    if w[i] == "table":
        print("Symbol Table\n")
        print("====================\n")
        for q in assignment:
            print(q, "\t", assignment[q], "\n")
    # Parse 'show'
    elif w[i] == "show":
        string = "value: "
        while True:
            if w[i] == "$":
                break
            i += 1
            value = exp()
            string += str(value) + "\t"
        print(string)
        print("Done")
    # Parse assignment operator 
    elif w[i].isalpha():
        temp = w[i]
        assignment[w[i]] = None
        i += 1
        if w[i] == "=":
            i += 1
            value = exp()
            assignment[temp] = value
        print("Done")

#---------------------------------------
# Parse an Expression   <exp> → <term>{+<term> | -<term>}
#---------------------------------------
def exp():
    global i, err

    value = term()
    while True:
        if w[i] == '+':
            i += 1
            value = binary_op('+', value, term())
        elif w[i] == '-':
            i += 1
            value = binary_op('-', value, term())
        else:
            break

    return value
#---------------------------------------
# Parse a Term   <term> → <factor>{+<factor> | -<factor>}
#---------------------------------------
def term():
    global i, err

    value = factor()
    while True:
        if w[i] == '*':
            i += 1
            if w[i] == "*":
                i += 1
                value = binary_op('**', value, factor())
            else:
                value = binary_op('*', value, factor())
        elif w[i] == '/':
            i += 1
            if w[i] == '/':
                i += 1
                value = binary_op('//', value, factor())
            else:    
                value = binary_op('/', value, factor())
        elif w[i] == "%":
            i += 1
            value = binary_op('%', value, factor())
        else:
            break

    return value
#---------------------------------------
# Parse a Factor   <factor> → (<exp>) | <number> 
#---------------------------------------       
def factor():
    global i, err
    value = None
    if w[i] == '(':
        i += 1          # read the next character
        value = exp()
        if w[i] == ')':
            i += 1
            return value
        else:
            print('missing )')
            raise ParseError
    elif w[i] == 'pi':
        i += 1
        return math.pi
    # elif w[i] == '-':
    #     i += 1
    #     return -factor()
    elif w[i] in func_map.keys():
        return func()
    else:
        try:
            value = atomic(w[i])
            i += 1          # read the next character
        except ValueError:
            value = assignment[w[i]]
            i += 1
            # print('number expected')
            # value = None
    
    #print('factor returning', value)
    
    if value == None: raise ParseError
    return value
#---------------------------------------
# Parse an Function   <exp> → <term>{+<term> | -<term>}
#---------------------------------------
def func():
    global i, err
    x = func_map[w[i]]
    i += 1
    value = factor()
    return x(value)


# Handle arithmetic operators
def binary_op(op, lhs, rhs):
    if func_map.get(op):
        return func_map[op](lhs, rhs)
    else: return None

# Return atomic
def atomic(x):
    return float(x)


def man():
    print('################################################################################')
    print('# Expression Parser - Designed by Anupam Krishna.                              #')
    print('#                                                                              #')
    print('# These commands are defined internally. Type   \'help\' to see this list:       #')
    print('# <id> = <exp>        -> assign integer/exp to variable                        #')
    print('#                     -> usage: a = 6.022 | b = 3.142 | etc = a - b + fact(4)  #')
    print('#                                                                              #')
    print('# show <exp>{,<exp>}  -> echo a variable, expression, or number.               #')
    print('#                     -> usage: show a | show a + b, | show etc                #')
    print('#                                                                              #')
    print('# table               -> display table of variables                            #')
    print('#                     -> usage: table                                          #')
    print('#                                                                              #')
    print('# Supported functions:                                                         #')
    print('#                     sin, cos, tan                                            #')
    print('#                     +,   -,   /                                              #')
    print('#                     //,  *,   %                                              #')
    print('#                     **, log, sqrt                                            #')
    print('#                     fact                                                     #')
    print('################################################################################')


#==============================================================
# BACK END PARSER (ACTION RULES)
#==============================================================
w = input('\n>> ')
while w != '':
    if w == 'exit':
        print('bye!')
        break
    elif w == 'help':
        man()
        
    #------------------------------
    # Split string into token list.
    #------------------------------
    for c in '()+-*/,':
        w = w.replace(c, ' '+c+' ')
    w = w.split()
    w.append('$') # EOF marker

    print('\nToken Stream:     ', end = '')
    for t in w: print(t, end = '  ')
    print('\n')
    i = 0
    try:
        print('Value:           ', statement()) # call the parser
    except:
        print('parse error')
    print()
    if w[i] != '$': print('Syntax error:')
    print('read | un-read:   ', end = '')
    for c in w[:i]: print(c, end = '')
    print(' | ', end = '')
    for c in w[i:]: print(c, end = '')
    print()
    w = input('\n\n>> ')
#print(w[:i], '|', w[i:])
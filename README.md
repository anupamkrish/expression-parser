## Interactive Recursive Descent Parser

First, we clone the project. Open a terminal and type
```
git clone https://gitlab.com/anupamkrish/expression-parser/
```

The program uses Python3 and above. To get started, navigate to the directory on the terminal and run program.
```
cd expression-parser
python exp-parser.py
```

To visit manual and see supported commands, type:
```
help
```

This program implements a recursive descent parser for the CFG below:
```
Syntax Rule  Lookahead Set          Strings generated
------------------------------------------------------------
<exp> → <term>{+<term> | -<term>}
<term> → <factor>{*<factor> | /<factor>}
<factor> → (<exp>) | pi | <func>(<exp>) | <atomic> # Commented out: -<factor>
<func> → sin | cos | tan | sqrt
<statement> → table | show <exp>{,<exp>} | <id> = <exp>
```
### EBNF grammar

```
<statement> → table | show <exp>{,<exp>} | <id> = <exp>
<exp> → <term>{+<term> | -<term>}
<term> → <factor>{*<factor> | /<factor> | %<factor> | **<factor> | //<factor>}
<factor> → (<exp>) | pi | <func>(<exp>) | <atomic>
<func> → sin | cos | tan | sqrt
```

In this grammar, &lt;atomic&gt; is either a numeric literal like 2.34, or it is a variable name, like x, mass, etc.

The program runs a loop, calling the statement() function on each line of user input.

The interactive session below is based on the above grammar. Here is a simulation of a session. The user input is what follows on a line after the >> prompt
```
>> x = 2
Done
>> y = 3
Done
>> show x, y, 1 + sqrt(2*x*y - 4)
value: 2.0
3.0
3.8284271247461903
Done
>> z = sqrt(2*x*y + 4)
Done
>> table
Symbol Table
====================
x 2.0
y 3.0
z 4.0
Done
>> show x + y*sin(pi*sqrt(z)/4)
value: 5.0
Done
>> x = (2 - 3
missing )
Parse Error
>>
```
